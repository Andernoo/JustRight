#  A nice, small image
FROM node:alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm install

# Copy the server files
COPY server .

# Expose our port
EXPOSE 9090

# Run the server - docker run -[it|d] -p 9090:9090 [image_name]
CMD [ "node", "server.js" ]