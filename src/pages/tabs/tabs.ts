import { Component } from '@angular/core';
import { Events } from 'ionic-angular';
import { UpdatePage } from '../update/update';
import { OverviewPage } from '../overview/overview';

@Component({
	templateUrl: 'tabs.html'
})
export class TabsPage {

	tab1Root = UpdatePage;
	tab2Root = OverviewPage;

	constructor(events: Events) {

	}
}
