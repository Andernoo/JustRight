'use strict';
module.exports = function (app, router) {
	var JustRightController = require('../controllers/JustRightController.js');

	var justRightController = new JustRightController.JustRightController();

	router.route('/details')
		.get(function (req, res) {
			res.json(justRightController.getDetails());
		});

	router.route('/vote')
		.post(function (req, res) {
			justRightController.addVote(req, res);
		});

	router.route('/authorise')
		.get(function (req, res) {
			justRightController.authorise(req, res);
		})
	app.use('/api/v1', router);
};